# LibreHealth Community Governance Documents

This repository contains governance documents for the LibreHealth Community. Unless otherwise mentioned, all documents in this repository are licensed under the Creative Commons 4.0 International Attribution License (CC BY 4.0).

Pull requests and comments about the documents are always welcome, either here in the repository or in our community forums.
